const express = require('express');
const passport = require('passport');
const usersControllers = require('../controllers/users');
const { upload } = require('../utils/upload');

const router = express.Router();

router.get('/', passport.authenticate('jwt', { session: false }), usersControllers.getAuthUser);

router.get('/:id', usersControllers.getUser);

router.put('/', passport.authenticate('jwt', { session: false }), upload, usersControllers.editUser);

module.exports = router;
