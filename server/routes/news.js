const express = require('express');

const passport = require('passport');

const { upload } = require('../utils/upload');

const router = express.Router();

const newsController = require('../controllers').news;

router.get('/', newsController.getNews);

router.post('/photo', passport.authenticate('jwt', { session: false }), upload, newsController.setNews);

router.post('/', passport.authenticate('jwt', { session: false }), upload, newsController.setNews);

router.get('/:id', newsController.getUserNews);

module.exports = router;
