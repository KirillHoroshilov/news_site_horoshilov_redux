const express = require('express');
const { upload } = require('../utils/upload');

const router = express.Router();
const controllersApi = require('../controllers').api;

router.post('/registration', upload, controllersApi.registration);
router.post('/login', controllersApi.login);

router.post('/google/auth', controllersApi.googleAuth);

module.exports = router;
