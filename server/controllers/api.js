const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('../models');

module.exports = {
  async registration(req, res) {
    const { name, email, password, valid } = req.body;
    const { path } = req.file || { path: 'null' };
    if (password !== valid) {
      res.status(400).json({
        field: 'valid',
        message: 'Passwords do not match',
      });
    }
    try {
      const salt = await bcrypt.genSalt(10);
      const hashPassword = await bcrypt.hash(password, salt);

      const user = await User.findOne({
        where: {
          email,
        },
      });

      if (user) {
        res.status(400).json({
          field: 'email',
          message: 'User  exists',
        });
      } else {
        const newUser = await User.create({
          name,
          email,
          password: hashPassword,
          avatar: path,
        });

        const token = jwt.sign({ user: newUser }, 'secret');

        res.status(201).json({
          token,
          user: newUser,
        });
      }
    } catch (error) {
      res.status(501).json({
        message: error.message,
      });
    }
  },

  async login(req, res) {
    const { email, password } = req.body;

    try {
      const user = await User.findOne({
        where: {
          email,
        },
      });

      if (!user) {
        res.status(400).json({
          field: 'email',
          message: 'No such user',
        });
      }

      if (!(await bcrypt.compare(password, user.password))) {
        res.status(400).json({
          field: 'password',
          message: 'Invalid password',
        });
      } else {
        const token = jwt.sign({ user }, 'secret');
        res.status(200).json({
          token,
          user,
        });
      }
    } catch (error) {
      res.status(501).json({ message: error.message });
    }
  },

  async googleAuth(req, res) {
    const { email, name, imageUrl } = req.body;

    const user = await User.findOne({
      where: {
        email,
      },
    });
    if (user) {
      const token = jwt.sign({ user }, 'secret');
      res.status(200).json({
        token,
        user,
      });
    } else {
      const newUser = await User.create({
        email,
        name,
        avatar: imageUrl,
        password: 'google',
      });
      const newToken = jwt.sign({ newUser }, 'secret');
      res.status(200).json({
        user: newUser,
        token: newToken,
      });
    }
  },
};
