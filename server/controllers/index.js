const news = require('./news');
const users = require('./users');
const api = require('./api');

module.exports = {
  news, api, users,
};
