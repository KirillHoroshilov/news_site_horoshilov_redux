const { News, User } = require('../models');

module.exports = {
  getNews(req, res) {
    return News.findAll({
      include: [{
        model: User,
        as: 'author',
      }],
    })
      .then((newsList) => res.status(200).send(newsList))
      .catch((error) => res.status(501).send({ message: `Database error: ${error.message}` }));
  },

  getUserNews(req, res) {
    return News.findAll({
      where: { author_id: req.params.id },
      include: [{
        model: User,
        as: 'author',
      }],
    }).then((news) => {
      res.status(200).send(news);
    })
      .catch(((error) => res.status(501).send({ message: `Database error ${error.message}` })));
  },

  setNews(req, res) {
    const { title, tags, text } = req.body;
    const { id } = req.user;
    if (req.file) {
      return News.create({
        title,
        tags,
        text,
        author_id: id,
        image: req.file.path,
      }).then((data) => res.status(200).send(data)).catch((error) => res.status(501).send(error));
    }
    return News.create({
      title,
      tags,
      text,
      author_id: id,
      image: null,
    }).then((data) => res.status(200).send(data)).catch((error) => res.status(501).send(error));
  },
};
