const { User } = require('../models');

module.exports = {
  getAuthUser(req, res) {
    res.status(200).send(req.user);
  },

  getUser(req, res) {
    return User.findOne({
      where: {
        id: req.params.id,
      },
    })
      .then((user) => {
        if (user) {
          res.status(200).send(user);
        } else {
          res.status(501).send('User does not exist');
        }
      })
      .catch((error) => res.status(501).send(error.message));
  },

  editUser(req, res) {
    const { name, email } = req.body;

    if (req.file) {
      return User.update(
        {
          name,
          email,
          avatar: req.file.path,
        },
        {
          where: {
            id: req.user.id,
          },
        },
      )
        .then(() => res.status(200).json(req.user.id))
        .catch((error) => res.status(501).send(error.message));
    }
    if (!email) {
      return User.update(
        {
          name,
        },
        {
          where: {
            id: req.user.id,
          },
        },
      )
        .then(() => res.status(200).json(req.user.id))
        .catch((error) => res.status(501).send(error.message));
    }
    if (!name) {
      return User.update(
        {
          email,
        },
        {
          where: {
            id: req.user.id,
          },
        },
      )
        .then(() => res.status(200).json(req.user.id))
        .catch((error) => res.status(501).send(error.message));
    }
    return User.update(
      {
        name,
        email,
      },
      {
        where: {
          id: req.user.id,
        },
      },
    )
      .then(() => res.status(200).json(req.user.id))
      .catch((error) => res.status(501).send(error.message));
  },
};
