const jwt = require('jsonwebtoken');
const { registration } = require('../controllers/api');

const mockedUsers = [
  {
    id: 1,
    name: 'Test name',
    email: 'Test email',
    password: 'Test password',
  },
];

jest.mock('../models', () => ({
  User: {
    findOne: jest.fn(({ where: { id } }) => {
      const currentUser = mockedUsers.find((item) => item.id === id);
      return Promise.resolve(currentUser ?? null);
    }),
    create: jest.fn(({ password, ...user }) => {
      mockedUsers.push(user);
      return Promise.resolve(user);
    }),
  },
}));

const response = {
  status: jest.fn(() => response),
  json: jest.fn(() => response),
};

describe('Testing api controller', () => {
  it('Should be return token and saved user', async () => {
    const request = {
      body: {
        name: 'Test name request',
        email: 'Test email request',
        password: 'Test password request',
        valid: 'Test password request',
      },
    };

    const { valid, password, ...userDto } = request.body;

    const token = jwt.sign({ user: { ...userDto, avatar: 'null' } }, 'secret');

    await registration(request, response);

    expect(response.status).toHaveBeenCalledWith(201);
    expect(mockedUsers[1]).toBeTruthy();
    expect(mockedUsers[1].name).toBe(userDto.name);
    expect(mockedUsers[1].email).toBe(userDto.email);
    expect(response.json).toHaveBeenCalledWith({ token, user: mockedUsers[1] });
  });
});
