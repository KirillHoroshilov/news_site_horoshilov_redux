const { getAuthUser, getUser, editUser } = require('../controllers/users');

const mockedUsers = [
  {
    id: 1,
    name: 'Test name',
    email: 'Test email',
  },
];

jest.mock('../models', () => ({
  User: {
    findOne: jest.fn(({ where: { id } }) => {
      const currentUser = mockedUsers.find((item) => item.id === id);
      return Promise.resolve(currentUser ?? null);
    }),
    update: jest.fn(({ name, email }, { where: { id } }) => {
      const currentUser = mockedUsers.find((item) => item.id === id);
      if (currentUser) {
        currentUser.email = email;
        currentUser.name = name;
        return Promise.resolve([2, 2]);
      }
      return Promise.resolve([2, 0]);
    }),
  },
}));

const response = {
  status: jest.fn(() => response),
  send: jest.fn(() => response),
  json: jest.fn(() => response),
};

describe('Testing users controller', () => {
  it('Should be return user by id', async () => {
    await getUser({ params: { id: mockedUsers[0].id } }, response);
    expect(response.status).toHaveBeenLastCalledWith(200);
    expect(response.send).toHaveBeenLastCalledWith(mockedUsers[0]);
  });

  it('Should be return error, when user does not exist', async () => {
    await getUser({ params: { id: 2 } }, response);
    expect(response.status).toHaveBeenLastCalledWith(501);
    expect(response.send).toHaveBeenLastCalledWith('User does not exist');
  });

  it('Should be return authorization user', async () => {
    const mockedAuthUser = {
      id: 1,
      name: 'Test auth user',
    };
    await getAuthUser({ user: mockedAuthUser }, response);
    expect(response.status).toHaveBeenLastCalledWith(200);
    expect(response.send).toHaveBeenLastCalledWith(mockedAuthUser);
  });

  it('Should be update user', async () => {
    const mockedName = 'Update name';
    const mockedEmail = 'Update email';

    await editUser(
      { body: { name: mockedName, email: mockedEmail }, user: { id: mockedUsers[0].id } },
      response,
    );
    expect(response.status).toHaveBeenLastCalledWith(200);
    expect(response.json).toHaveBeenLastCalledWith(mockedUsers[0].id);
    expect(mockedUsers[0].name).toBe(mockedName);
    expect(mockedUsers[0].email).toBe(mockedEmail);
  });
});
