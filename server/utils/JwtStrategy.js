const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJWt = require('passport-jwt').ExtractJwt;
const { User } = require('../models');

module.exports = function strategy(passport) {
  const options = {
    jwtFromRequest: ExtractJWt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'secret',
  };

  passport.use(new JwtStrategy(options, (jwtPayload, done) => {
    User.findOne({ where: { id: jwtPayload.user.id } })
      .then((user) => done(null, user))
      .catch((error) => done(error));
  }));
};
