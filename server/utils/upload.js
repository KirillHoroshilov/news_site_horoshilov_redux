const multer = require('multer');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    try {
      cb(null, 'public/images/');
    } catch (error) {
      console.log('error', error);
      cb(error);
    }
  },
  filename: (req, file, cb) => {
    try {
      cb(null, new Date().toISOString() + file.originalname);
    } catch (error) {
      console.log('error', error);

      cb(error);
    }
  },
});

module.exports.upload = (req, res, next) => {
  console.log('=======================', !req.body.title && !req.body.name, '==========================');
  if (!req.body.title && !req.body.name) {
    multer({
      storage,
    }).single('photo')(req, res, next, (error) => {
      if (error) {
        console.log(error);
      }
    });
  } else {
    next();
  }
};
