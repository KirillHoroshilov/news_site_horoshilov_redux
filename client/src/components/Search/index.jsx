import React from 'react';
import PropTypes from 'prop-types';
import { Button, MenuItem, Select, TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import './index.css';

const Search = (props) => {
  const { search, setSearch, filter, setFilter } = props;

  const changeHandlerInput = (event) => {
    setSearch(event.target.value);
  };

  const changeHandlerFilter = (event) => {
    setFilter(event.target.value || event.target.textContent);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    setSearch('');
  };

  return (
    <div className="search" data-testid="search">
      <form data-testid="searchForm" action="submit" className="search__form" onSubmit={onSubmit}>
        <TextField
          data-testid="searchInput"
          className="search__input"
          label={`Search: ${filter}`}
          id="outlined-basic"
          value={search}
          onChange={changeHandlerInput}
        />
        <Button type="submit">
          <SearchIcon />
        </Button>
      </form>
      <div className="search__buttons">
        <Select data-testid="select" value={filter} onChange={changeHandlerFilter}>
          <MenuItem value="All">All</MenuItem>
          <MenuItem value="Title">Title</MenuItem>
          <MenuItem value="Text">Text</MenuItem>
          <MenuItem value="Tags">Tags</MenuItem>
          <MenuItem value="Name">Name</MenuItem>
        </Select>
      </div>
    </div>
  );
};

Search.propTypes = {
  search: PropTypes.string.isRequired,
  filter: PropTypes.string.isRequired,
  setSearch: PropTypes.func.isRequired,
  setFilter: PropTypes.func.isRequired,
};

export default Search;
