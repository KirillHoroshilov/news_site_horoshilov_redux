import React from 'react';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Chip,
  Typography,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import './index.css';
import { Link } from 'react-router-dom';
import { getPath } from '../../helpers';

const News = ({ news }) => (
  <Card data-testid="news" className="news-page news">
    <CardActionArea>
      <CardMedia
        data-testid="image"
        component="img"
        height="200"
        image={getPath(news.image) || '/default.jpg'}
      />
    </CardActionArea>
    <CardContent className="news__main">
      <Accordion>
        <AccordionSummary
          className="news__title"
          aria-controls="panel2d-content"
          id="panel2d-header"
        >
          <Typography data-testid="title" className="news__title">
            Title post: {news.title}
          </Typography>
        </AccordionSummary>
        <AccordionDetails className="news__text">
          <Typography data-testid="text">{news.text}</Typography>
        </AccordionDetails>
      </Accordion>
      <Typography className="news__author">
        Author:{' '}
        <Link data-testid="link" to={`/user/${news.author.id}`}>
          {news.author.name}
        </Link>
      </Typography>
      <div className="news__tags">
        {news.tags.split(',').map((item) => (
          <Chip data-testid="tags" key={item} label={item} />
        ))}
      </div>
    </CardContent>
  </Card>
);

News.propTypes = {
  news: PropTypes.shape({
    title: PropTypes.string,
    text: PropTypes.string,
    tags: PropTypes.string,
    image: PropTypes.string,
    author: PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.number,
    }),
  }),
};

News.defaultProps = {
  news: {
    title: '',
    text: '',
    tags: '',
    author: {
      name: '',
      id: 0,
    },
    image: '',
  },
};

export default News;
