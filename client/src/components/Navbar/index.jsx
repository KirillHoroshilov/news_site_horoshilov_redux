import { AppBar, Toolbar, Typography, Button, Avatar } from '@material-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropType from 'prop-types';
import { Link, useHistory } from 'react-router-dom';
import actionCreator from '../../redux/actions/actions';
import { DELETE_USER } from '../../redux/constant';
import Search from '../Search';
import './index.css';
import { getPath } from '../../helpers';

const Navbar = ({ search, setSearch, filter, setFilter, searchFlag }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const user = useSelector((state) => state.user.user);

  const logout = () => {
    dispatch(actionCreator(DELETE_USER));
    history.push('/');
  };

  return (
    <AppBar position="static" data-testid="navbar">
      <Toolbar className="navbar">
        <Link to="/" className="navbar__title">
          <Typography variant="h4">News site</Typography>
        </Link>
        {searchFlag ? (
          <Search search={search} setSearch={setSearch} filter={filter} setFilter={setFilter} />
        ) : (
          <Typography data-testid="greeting" className="navbar__hello">
            Hello {user.name}
          </Typography>
        )}
        <div className="navbar__links">
          {user.name ? (
            <>
              {' '}
              <div className={searchFlag ? 'logout' : 'login'}>
                <Link to={`/user/${user.id}`} data-testid="userLink">
                  <Avatar src={getPath(user.avatar) || '/default.jpg'} />
                </Link>
                <Button data-testid="logout" onClick={logout}>
                  Logout
                </Button>
              </div>
            </>
          ) : (
            <>
              {' '}
              <Link data-testid="registration" to="/registration">
                <Button>Registration</Button>
              </Link>
              <Link data-testid="login" to="/login">
                <Button>Login</Button>
              </Link>
            </>
          )}
        </div>
      </Toolbar>
    </AppBar>
  );
};

Navbar.propTypes = {
  search: PropType.string,
  setSearch: PropType.func,
  filter: PropType.string,
  setFilter: PropType.func,
  searchFlag: PropType.bool,
};

Navbar.defaultProps = {
  search: '',
  setSearch: () => '',
  filter: '',
  setFilter: () => '',
  searchFlag: true,
};

export default Navbar;
