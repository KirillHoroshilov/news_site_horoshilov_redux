import React from 'react';
import { Button, Dialog, DialogTitle } from '@material-ui/core';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { renderFileInput, renderInput, renderTextArea } from '../../../../helpers';
import './index.css';
import { newsValidate } from '../../helpers';

const NewsModal = ({ open, handleClose, handleSubmit }) => (
  <Dialog open={open}>
    <div className="modal">
      <DialogTitle className="modal__title">New news</DialogTitle>
      <form action="submit" onSubmit={handleSubmit} className="modal__form">
        <div className="modal__inputs">
          <Field name="title" label="Title" type="text" component={renderInput} />
          <Field name="tags" label="Tags" type="text" component={renderInput} />
        </div>
        <Field name="text" label="Text" type="text" component={renderTextArea} />
        <Field name="photo" label="Photo" type="file" component={renderFileInput} />
        <div className="modal__actions">
          <Button type="submit">Enter</Button>
          <Button onClick={handleClose}>Close</Button>
        </div>
      </form>
    </div>
  </Dialog>
);

NewsModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({ form: 'newNews', validate: newsValidate })(NewsModal);
