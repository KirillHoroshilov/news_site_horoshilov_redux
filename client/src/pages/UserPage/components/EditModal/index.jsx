import React from 'react';
import { Button, Dialog, DialogTitle } from '@material-ui/core';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { renderFileInput, renderInput } from '../../../../helpers';
import { editValidate } from '../../helpers';

const EditModal = ({ open, handleClose, handleSubmit }) => (
  <Dialog open={open}>
    <div className="modal">
      <DialogTitle className="modal__title">Edit profile</DialogTitle>
      <form action="submit" onSubmit={handleSubmit} className="modal__form">
        <div className="modal__inputs">
          <Field name="name" label="New name" type="text" component={renderInput} />
          <Field name="email" label="New email" type="text" component={renderInput} />
          <Field name="photo" label="Avatar" type="file" component={renderFileInput} />
        </div>
        <div className="modal__actions">
          <Button type="submit">Enter</Button>
          <Button onClick={handleClose}>Close</Button>
        </div>
      </form>
    </div>
  </Dialog>
);

EditModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({ form: 'editForm', validate: editValidate })(EditModal);
