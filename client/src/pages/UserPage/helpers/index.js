import { change, untouch } from 'redux-form';

const trim = (string) => string && string.trim();

export const editValidate = (values) => {
  const errors = {};

  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email) && !values.name) {
    errors.email = 'Invalid email address';
  }

  return errors;
};

export const newsValidate = (values) => {
  const errors = {};

  if (!trim(values.title)) {
    errors.title = 'Required';
  }
  if (!trim(values.tags)) {
    errors.tags = 'Required ';
  }
  if (!trim(values.text)) {
    errors.title = 'Text required';
  }

  return errors;
};

export const resetFields = (formName, fieldsObj, dispatch) => {
  Object.keys(fieldsObj).forEach((fieldKey) => {
    dispatch(change(formName, fieldKey, ''));
    dispatch(untouch(formName, fieldKey));
  });
};
