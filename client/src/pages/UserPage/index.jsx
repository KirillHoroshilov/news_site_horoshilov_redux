import { Avatar, Button, Card, CardActions, CardContent, Typography } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Navbar from '../../components/Navbar';
import News from '../../components/News';
import { getPath } from '../../helpers';
import actionCreator from '../../redux/actions/actions';
import { EDIT_USER, FETCH_USER_PAGE, SET_NEWS } from '../../redux/constant';
import EditModal from './components/EditModal';
import NewsModal from './components/NewsModal';
import { resetFields } from './helpers';
import './index.css';

const UserPage = () => {
  const dispatch = useDispatch();

  const [openNewsModal, setOpenNewsModal] = useState(false);
  const [openEditModal, setOpenEditModal] = useState(false);
  const user = useSelector((state) => state.user.user);

  const userPage = useSelector((state) => state.userPage.user);

  useEffect(() => {
    dispatch(actionCreator(FETCH_USER_PAGE, user.id));
  }, [user.id]);

  const handleOpenNewsModal = () => setOpenNewsModal(true);

  const handleCloseNewsModal = () => setOpenNewsModal(false);

  const handleOpenEditModal = () => setOpenEditModal(true);

  const handleCloseEditModal = () => setOpenEditModal(false);

  const onSubmitNewsModal = (values) => {
    if (values.photo) {
      const formData = new FormData();
      Object.entries(values).map(([key, value]) => formData.append(key, value));
      dispatch(actionCreator(SET_NEWS, formData));
    } else {
      dispatch(actionCreator(SET_NEWS, values));
    }

    resetFields('newNews', values, dispatch);
    handleCloseNewsModal();
  };

  const onSubmitEditModal = (values) => {
    if (values.photo) {
      const formData = new FormData();
      Object.entries(values).map(([key, value]) => formData.append(key, value));
      dispatch(actionCreator(EDIT_USER, formData));
    } else {
      dispatch(actionCreator(EDIT_USER, values));
    }

    resetFields('editForm', values, dispatch);
    handleCloseEditModal();
  };

  return (
    <>
      <Navbar searchFlag={false} />
      <Card className="user__card">
        <CardContent className="user__info">
          <Avatar
            style={{ width: '150px', height: '150px' }}
            src={getPath(userPage.avatar) || '/default.jpg'}
          />
          <div className="user_description">
            <Typography>Name: {userPage.name}</Typography>
            <Typography>Email: {userPage.email}</Typography>
            <Typography>Total news {userPage.news ? userPage.news.length : 0}</Typography>
          </div>
        </CardContent>
        {userPage.id === user.id ? (
          <CardActions>
            <Button onClick={handleOpenEditModal}>Edit</Button>
            <EditModal
              onSubmit={onSubmitEditModal}
              handleClose={handleCloseEditModal}
              open={openEditModal}
            />
            <Button onClick={handleOpenNewsModal}>New news</Button>
            <NewsModal
              onSubmit={onSubmitNewsModal}
              handleClose={handleCloseNewsModal}
              open={openNewsModal}
            />
          </CardActions>
        ) : null}
      </Card>
      <div className="user__news">
        {userPage.news && userPage.news.length ? (
          userPage.news.map((item) => <News key={item.id} news={item} />)
        ) : (
          <h1>Not news</h1>
        )}
      </div>
    </>
  );
};

export default UserPage;
