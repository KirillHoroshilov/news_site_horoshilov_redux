import MAX_ELEMENTS from './constant';

function includes(object, string, type) {
  switch (type) {
    case 'Title':
      return string ? object.title.includes(string) : true;
    case 'Text':
      return string ? object.text.includes(string) : true;
    case 'Tags':
      return string ? object.tags.includes(string) : true;
    case 'Name':
      return string ? object.author.name.includes(string) : true;
    default:
      return string
        ? object.title.includes(string) ||
            object.text.includes(string) ||
            object.tags.includes(string) ||
            object.author.name.includes(string)
        : true;
  }
}

function pruning(array, offset) {
  if (array.length < 5) {
    const rightBorder = Math.ceil(array.length / 4) * MAX_ELEMENTS;
    const leftBorder = rightBorder - MAX_ELEMENTS;
    return array.slice(leftBorder, rightBorder);
  }
  const rightBorder = offset * MAX_ELEMENTS;
  const leftBorder = rightBorder - MAX_ELEMENTS;
  return array.slice(leftBorder, rightBorder);
}

export default function filterNews(array, string, type, offset) {
  const arrayRequired = pruning(
    array.filter((item) => includes(item, string, type)),
    Number(offset),
  );
  return arrayRequired;
}
