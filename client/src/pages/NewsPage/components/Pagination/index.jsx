import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';
import MAX_ELEMENTS from '../../helpers/constant';
import './index.css';

const Pagination = (props) => {
  const { page, setPage, totalNews } = props;
  const totalPages = useMemo(() => Math.ceil(totalNews / MAX_ELEMENTS), [totalNews]);

  const changeHandlerPage = (event) => {
    setPage(event.target.name || event.target.textContent);
  };

  const paginationNode = Array.from({ length: totalPages }, (item, index) => index).map((item) => {
    if (Number(item) + 1 === page) {
      return (
        <Button
          key={item}
          name={item + 1}
          color="secondary"
          variant="contained"
          onClick={changeHandlerPage}
        >
          {item + 1}
        </Button>
      );
    }
    return (
      <Button
        key={item}
        name={item + 1}
        color="primary"
        variant="contained"
        onClick={changeHandlerPage}
      >
        {item + 1}
      </Button>
    );
  });

  return <div className="pagination">{paginationNode}</div>;
};

Pagination.propTypes = {
  page: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
  totalNews: PropTypes.number.isRequired,
};

export default Pagination;
