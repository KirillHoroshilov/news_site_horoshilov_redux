import React, { useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import News from '../../components/News';
import Pagination from './components/Pagination';
import filterNews from './helpers';
import './index.css';
import Navbar from '../../components/Navbar';

const NewsPage = () => {
  const news = useSelector((state) => state.news.newsList);
  const [search, setSearch] = useState('');
  const [filter, setFilter] = useState('All');
  const [page, setPage] = useState(1);
  const filteredNews = useMemo(
    () => filterNews(news, search, filter, page),
    [search, news, filter, page],
  );

  return (
    <>
      <Navbar search={search} setSearch={setSearch} filter={filter} setFilter={setFilter} />
      <div className="news-page">
        <div className="news-page__list">
          {filteredNews.length ? (
            filteredNews.map((item) => <News key={item.id} news={item} />)
          ) : (
            <h1>Don`t news</h1>
          )}
        </div>
        {filteredNews.length ? (
          <Pagination page={Number(page)} setPage={setPage} totalNews={news.length} />
        ) : null}
      </div>
    </>
  );
};

export default NewsPage;
