import React from 'react';
import { Card, CardActions, CardContent, Typography, Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { useLocation } from 'react-router';
import { useDispatch } from 'react-redux';
import GoogleLogin from 'react-google-login';
import validate from './helpers';
import { renderFileInput, renderInput } from '../../helpers';
import './index.css';
import actionCreator from '../../redux/actions/actions';
import { GOOGLE_AUTH } from '../../redux/constant';
import Navbar from '../../components/Navbar';

const AuthorizationPage = (props) => {
  const { handleSubmit } = props;
  const dispatch = useDispatch();
  const location = useLocation();

  const responseGoogle = (response) => {
    const { profileObj } = response;
    dispatch(actionCreator(GOOGLE_AUTH, profileObj));
  };

  if (location.pathname === '/registration') {
    return (
      <>
        <Navbar searchFlag={false} />
        <div className="registration">
          <Card className="registration__form">
            <form action="submit" onSubmit={handleSubmit}>
              <CardContent className="registration__content">
                <Typography variant="h4" className="registration__title">
                  Registration
                </Typography>
              </CardContent>
              <CardActions className="registration__content">
                <Field name="name" label="Your name" type="text" component={renderInput} />
                <Field name="email" label="Your email" type="email" component={renderInput} />
                <Field
                  name="password"
                  label="Your password"
                  type="password"
                  component={renderInput}
                />
                <Field
                  name="valid"
                  label="Repeat your password"
                  type="password"
                  component={renderInput}
                />
                <Field name="photo" label="Your avatar" type="file" component={renderFileInput} />
                <Button type="submit">Registration</Button>
              </CardActions>
            </form>
          </Card>
        </div>
      </>
    );
  }
  if (location.pathname === '/login') {
    return (
      <>
        <Navbar searchFlag={false} />
        <div className="registration">
          <Card className="registration__form">
            <form action="submit" onSubmit={handleSubmit}>
              <CardContent className="registration__content">
                <Typography variant="h4" className="registration__title">
                  Login
                </Typography>
              </CardContent>
              <CardActions className="registration__content">
                <Field name="email" label="Your email" type="email" component={renderInput} />
                <Field
                  name="password"
                  label="Your password"
                  type="password"
                  component={renderInput}
                />
                <Button type="submit">Login</Button>
                <GoogleLogin
                  clientId="835167952643-a1q1ahnf654tvla2jo52a92c6k1vf076.apps.googleusercontent.com"
                  buttonText="Login Google"
                  onSuccess={responseGoogle}
                  onFailure={responseGoogle}
                  cookiePolicy="single_host_origin"
                />
              </CardActions>
            </form>
          </Card>
        </div>
      </>
    );
  }
  return null;
};

AuthorizationPage.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({ form: 'user', validate })(AuthorizationPage);
