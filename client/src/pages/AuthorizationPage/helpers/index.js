const validate = (values) => {
  const errors = {};
  if (!values.name) {
    errors.name = 'Required';
  }
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length < 8) {
    errors.password = 'Must be 8 characters or less';
  } else if (!/^(?=.*[a-z,A-Z])/.test(values.password)) {
    errors.password = 'Use at least one lowercase letter';
  } else if (!/^(?=.*[0-9])/.test(values.password)) {
    errors.password = 'Use at least one digit';
  }
  if (!values.valid) {
    errors.valid = 'Required';
  } else if (values.password !== values.valid) {
    errors.valid = 'Passwords do not match ';
  }
  return errors;
};
export default validate;
