import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import NewsPage from './pages/NewsPage';
import './App.css';
import AuthorizationPage from './pages/AuthorizationPage';
import actionCreator from './redux/actions/actions';
import { FETCH_NEWS, FETCH_USER, FETCH_USER_NEWS, LOGIN, REGISTRATION } from './redux/constant';
import UserPage from './pages/UserPage';
import axiosAdapter from './redux/helpers/axiosAdapter';

function App() {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.user);

  const submit = (values) =>
    values.valid
      ? dispatch(actionCreator(REGISTRATION, values))
      : dispatch(actionCreator(LOGIN, values));

  useEffect(() => {
    if (localStorage.getItem('token')) {
      axiosAdapter.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem(
        'token',
      )}`;
      dispatch(actionCreator(FETCH_USER, localStorage.getItem('token')));
    }
    if (user.id) {
      dispatch(actionCreator(FETCH_USER_NEWS, user.id));
    }
    dispatch(actionCreator(FETCH_NEWS));
  }, [user.id]);

  return (
    <>
      <Router>
        <Switch>
          <Route path="/" exact>
            <NewsPage />
          </Route>
          <Route path="/:authorization" exact>
            {user.name ? <Redirect to="/" /> : null}
            <AuthorizationPage onSubmit={submit} />
          </Route>
          <Route path={`/user/${user.id}`} exact>
            <UserPage />
          </Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
