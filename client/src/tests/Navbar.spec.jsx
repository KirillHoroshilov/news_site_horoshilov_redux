import React from 'react';
import Navbar from '../components/Navbar';
import { renderWithReduxAndRouter } from './helpers';

const initialState = {
  user: {
    user: {
      id: 1,
      name: 'Test',
    },
  },
};

describe('Testing Navbar Component', () => {
  it('Should be rendered', () => {
    const { getByTestId } = renderWithReduxAndRouter(<Navbar />);
    expect(getByTestId('navbar')).toBeTruthy();
  });

  it('Should be a link to the users page with the desired id', () => {
    const { getByTestId } = renderWithReduxAndRouter(<Navbar />, {
      initialState,
    });
    const link = getByTestId('userLink');
    expect(link.getAttribute('href')).toBe(`/user/${initialState.user.user.id}`);
  });

  it('Should have a logout button if there is a user', () => {
    const { getByTestId } = renderWithReduxAndRouter(<Navbar />, {
      initialState,
    });
    const logoutButton = getByTestId('logout');
    expect(logoutButton).toBeTruthy();
  });

  it('The user should be deleted from state if there was a click on the logout button', () => {
    const { getByTestId, store } = renderWithReduxAndRouter(<Navbar />, {
      initialState,
    });
    const previousState = store.getState();
    expect(previousState.user.user).toBe(initialState.user.user);
    const logoutButton = getByTestId('logout');
    logoutButton.click();
    const nextState = store.getState();
    expect(nextState.user.user).toEqual({});
  });

  it('Should be links to register and login if there is no user', () => {
    const { getByTestId } = renderWithReduxAndRouter(<Navbar />);
    const registrationLink = getByTestId('registration');
    const loginLink = getByTestId('login');
    expect(registrationLink.getAttribute('href')).toBe('/registration');
    expect(loginLink.getAttribute('href')).toBe('/login');
  });
});
