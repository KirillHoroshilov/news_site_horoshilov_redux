import React from 'react';
import Search from '../components/Search';
import { renderWithReduxAndRouter } from './helpers';

const mockSearch = 'Test';
const mockedSetSearch = jest.fn();
const mockFilter = 'All';
const mockedSetFilter = jest.fn();

describe('Testing Search Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = renderWithReduxAndRouter(
      <Search
        search={mockSearch}
        setSearch={mockedSetSearch}
        filter={mockFilter}
        setFilter={mockedSetFilter}
      />,
    );
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it('Should be rendered', () => {
    const { getByTestId } = wrapper;
    expect(getByTestId('search')).toBeTruthy();
  });

  it('Should display correctly search value from provided variable', () => {
    const { getByTestId } = wrapper;
    const searchElement = getByTestId('searchInput');
    const inputElement = searchElement.querySelector('input');
    expect(inputElement.value).toBe(mockSearch);
  });

  it('Should display correctly search filter from provided variable', () => {
    const { getByTestId } = wrapper;
    const selectElement = getByTestId('select');
    expect(selectElement.textContent).toBe(mockFilter);
  });
});
