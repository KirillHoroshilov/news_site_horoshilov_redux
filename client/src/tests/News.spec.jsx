import React from 'react';
import News from '../components/News';
import { renderWithReduxAndRouter } from './helpers';

const mockNews = {
  title: 'Test title',
  text: 'Test text',
  tags: 'Tag1,Tag2',
  image: '/test/image',
  author: {
    id: 1,
    name: 'Test user',
  },
};

describe('Test News component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = renderWithReduxAndRouter(<News news={mockNews} />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it('Should be rendered', () => {
    const { getByTestId } = wrapper;
    expect(getByTestId('news')).toBeTruthy();
  });

  it('Should be news display correctly from the provided object', () => {
    const { getByTestId, getAllByTestId } = wrapper;
    const titleElement = getByTestId('title');
    expect(titleElement.textContent).toBe(`Title post: ${mockNews.title}`);
    const textElement = getByTestId('text');
    expect(textElement.textContent).toBe(mockNews.text);

    const tagsElements = getAllByTestId('tags');

    tagsElements.forEach((element, index) => {
      expect(element.textContent).toBe(mockNews.tags.split(',')[index]);
    });

    const linkElement = getByTestId('link');
    expect(linkElement.getAttribute('href')).toBe(`/user/${mockNews.author.id}`);
    expect(linkElement.textContent).toBe(mockNews.author.name);
  });
});
