import { testSaga } from 'redux-saga-test-plan';
import { fetchNews, getUser, login, registration } from '../redux/saga/sagas';
import actionCreator from '../redux/actions/actions';
import axiosAdapter from '../redux/helpers/axiosAdapter';
import { GET_NEWS, GET_USER, FETCH_USER_NEWS } from '../redux/constant';
import registrationFromAvatar from '../redux/helpers';

const mockedData = {
  user: {
    id: 1,
    name: 'Testing name',
  },
  token: 'Testing token',
  id: 2,
};

describe('Testing redux-saga', () => {
  it('Should be fetching news', () => {
    testSaga(fetchNews)
      .next()
      .call(axiosAdapter, 'http://localhost:3001/news/')
      .next({ data: mockedData })
      .put(actionCreator(GET_NEWS, mockedData))
      .next()
      .isDone();
  });
  it('Should be fetching user', () => {
    testSaga(getUser)
      .next()
      .call(axiosAdapter, 'http://localhost:3001/users/')
      .next({ data: mockedData })
      .put(actionCreator(GET_USER, mockedData))
      .next()
      .put(actionCreator(FETCH_USER_NEWS, mockedData.id))
      .next()
      .isDone();
  });
  it('Should be registration', () => {
    testSaga(registration, { payload: mockedData.user })
      .next()
      .call(axiosAdapter, 'http://localhost:3001/api/registration', {
        method: 'POST',
        data: registrationFromAvatar(mockedData.user),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      })
      .next({ data: mockedData })
      .put(actionCreator(GET_USER, mockedData.user))
      .next()
      .isDone();
  });

  it('Should be login', () => {
    testSaga(login, { payload: mockedData.user })
      .next()
      .call(axiosAdapter, 'http://localhost:3001/api/login', {
        method: 'POST',
        data: mockedData.user,
      })
      .next({ data: mockedData })
      .put(actionCreator(GET_USER, mockedData.user))
      .next()
      .isDone();
  });
});
