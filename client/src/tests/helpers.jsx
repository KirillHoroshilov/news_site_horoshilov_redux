import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { Router } from 'react-router';
import { createMemoryHistory } from 'history';
import rootReducer from '../redux/reducers/rootReducer';

export const createTestingStore = (initialState) => createStore(rootReducer, initialState);

export const renderWithReduxAndRouter = (
  ui,
  { initialState, store = createTestingStore(initialState) } = {},
) => {
  const history = createMemoryHistory();
  return {
    ...render(
      <Provider store={store}>
        <Router history={history}>{ui}</Router>
      </Provider>,
    ),
    store,
    history,
  };
};
