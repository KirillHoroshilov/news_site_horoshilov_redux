import { DELETE_USER, GET_NEWS, GET_USER } from '../redux/constant';
import rootReducer from '../redux/reducers/rootReducer';
import { createTestingStore } from './helpers';

const initialState = {
  form: {},
  news: {
    newsList: [],
  },
  user: {
    user: {},
  },
  userPage: {
    user: {},
  },
};

const createMockedAction = (type) => ({
  type,
  payload: { data: 'Testing Data' },
});

describe('Testing redux', () => {
  let state;

  beforeEach(() => {
    state = createTestingStore(initialState).getState();
  });

  it('Should return the initial state in the absence of actions', () => {
    const newState = rootReducer(state, {});
    expect(newState).toEqual(initialState);
  });

  it('Should return state with changed news', () => {
    const mockedAction = createMockedAction(GET_NEWS);
    expect(state.news.newsList).toEqual([]);
    const newState = rootReducer(state, mockedAction);
    expect(newState.news.newsList).toEqual(mockedAction.payload);
  });

  it('Should return state with changed user', () => {
    const mockedAction = createMockedAction(GET_USER);
    const newState = rootReducer(state, mockedAction);
    expect(newState.user.user).toEqual(mockedAction.payload);
  });

  it('Should return state without user', () => {
    const mockedActionGetUser = createMockedAction(GET_USER);
    const previousState = rootReducer(state, mockedActionGetUser);
    expect(previousState.user.user).toEqual(mockedActionGetUser.payload);
    const newState = rootReducer(state, {
      type: DELETE_USER,
    });
    expect(newState.user.user).toEqual({});
  });
});
