import axios from 'axios';

const AXIOS_TIMEOUT = 1e4;

export default axios.create({
  baseURL: 'http://localhost:3000',
  headers: { 'content-type': 'application/json' },
  timeout: AXIOS_TIMEOUT,
});
