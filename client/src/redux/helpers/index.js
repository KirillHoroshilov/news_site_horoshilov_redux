const registrationFromAvatar = (values) => {
  if (!values.photo) {
    return values;
  }
  const formData = new FormData();
  Object.entries(values).map(([key, value]) => formData.append(key, value));
  return formData;
};

export default registrationFromAvatar;
