export const GET_NEWS = 'GET_NEWS';
export const SET_NEWS = 'SET_NEWS';
export const FETCH_NEWS = 'FETCH_NEWS';

export const REGISTRATION = 'REGISTRATION';
export const LOGIN = 'LOGIN';
export const GOOGLE_AUTH = 'GOOGLE_AUTH';

export const GET_USER = 'GET_USER';
export const FETCH_USER = 'FETCH_USER';
export const DELETE_USER = 'DELETE_USER';
export const EDIT_USER = 'EDIT_USER';

export const FETCH_USER_NEWS = 'FETCH_USER_NEWS';
export const GET_USER_NEWS = 'GET_USER_NEWS';

export const SHOW_WARNING = 'SHOW_WARNING';
export const SET_WARNING = 'SET_WARNING';
export const HIDE_WARNING = 'HIDE_WARNING';

export const GET_USER_PAGE = 'GET_USER_PAGE';
export const FETCH_USER_PAGE = 'FETCH_USER_PAGE';

export const FETCH_USER_PAGE_NEWS = 'FETCH_USER_PAGE_NEWS';
export const GET_USER_PAGE_NEWS = 'GET_USER_PAGE_NEWS';
