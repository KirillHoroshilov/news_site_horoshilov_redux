import { fork, put, takeEvery, call } from 'redux-saga/effects';
import { updateSyncErrors } from 'redux-form';
import {
  GET_NEWS,
  FETCH_NEWS,
  REGISTRATION,
  LOGIN,
  GET_USER,
  FETCH_USER,
  FETCH_USER_NEWS,
  GET_USER_NEWS,
  SET_NEWS,
  EDIT_USER,
  GOOGLE_AUTH,
  FETCH_USER_PAGE,
  FETCH_USER_PAGE_NEWS,
  GET_USER_PAGE,
  GET_USER_PAGE_NEWS,
} from '../constant';
import actionCreator from '../actions/actions';
import axiosAdapter from '../helpers/axiosAdapter';
import registrationFromAvatar from '../helpers';

export function* fetchNews() {
  const { data } = yield call(axiosAdapter, 'http://localhost:3001/news/');
  yield put(actionCreator(GET_NEWS, data));
}

export function* setNews(action) {
  const {
    data: { author_id: id },
  } = yield call(axiosAdapter, 'http://localhost:3001/news/', {
    method: 'POST',
    data: action.payload,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
  });
  yield put(actionCreator(FETCH_NEWS));
  yield put(actionCreator(FETCH_USER_NEWS, id));
  yield put(actionCreator(FETCH_USER_PAGE_NEWS, id));
}

function* watchFetchNews() {
  yield takeEvery(FETCH_NEWS, fetchNews);
  yield takeEvery(SET_NEWS, setNews);
}

export function* registration(action) {
  try {
    const {
      data: { user, token },
    } = yield call(axiosAdapter, 'http://localhost:3001/api/registration', {
      method: 'POST',
      data: registrationFromAvatar(action.payload),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    });

    axiosAdapter.defaults.headers.common.Authorization = `Bearer ${token}`;
    localStorage.setItem('token', token);
    yield put(actionCreator(GET_USER, user));
  } catch (error) {
    const { field, message } = error.response.data;
    yield put(updateSyncErrors('user', { [field]: message }));
  }
}

export function* login(action) {
  try {
    const {
      data: { user, token },
    } = yield call(axiosAdapter, 'http://localhost:3001/api/login', {
      method: 'POST',
      data: action.payload,
    });
    axiosAdapter.defaults.headers.common.Authorization = `Bearer ${token}`;
    localStorage.setItem('token', token);
    yield put(actionCreator(GET_USER, user));
  } catch (error) {
    const { field, message } = error.response.data;
    yield put(updateSyncErrors('user', { [field]: message }));
  }
}

export function* googleAuthorization(action) {
  const {
    data: { user, token },
  } = yield call(axiosAdapter, 'http://localhost:3001/api/google/auth', {
    method: 'POST',
    data: action.payload,
  });
  axiosAdapter.defaults.headers.common.Authorization = `Bearer ${token}`;
  localStorage.setItem('token', token);
  yield put(actionCreator(GET_USER, user));
}

function* watchAuthorization() {
  yield takeEvery(REGISTRATION, registration);
  yield takeEvery(LOGIN, login);
  yield takeEvery(GOOGLE_AUTH, googleAuthorization);
}

export function* getUser() {
  const { data } = yield call(axiosAdapter, 'http://localhost:3001/users/');
  yield put(actionCreator(GET_USER, data));

  yield put(actionCreator(FETCH_USER_NEWS, data.id));
}

export function* getUserNews(action) {
  const { data } = yield call(axiosAdapter, `http://localhost:3001/news/${action.payload}`);
  yield put(actionCreator(GET_USER_NEWS, data));
}

export function* getPageUser(action) {
  const { data } = yield call(axiosAdapter, `http://localhost:3001/users/${action.payload}`);
  yield put(actionCreator(GET_USER_PAGE, data));

  yield put(actionCreator(FETCH_USER_PAGE_NEWS, data.id));
}

export function* getPageUserNews(action) {
  const { data } = yield call(axiosAdapter, `http://localhost:3001/news/${action.payload}`);
  yield put(actionCreator(GET_USER_PAGE_NEWS, data));
}

export function* editUser(action) {
  const { data } = yield call(axiosAdapter, 'http://localhost:3001/users/', {
    method: 'PUT',
    data: action.payload,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
  });
  yield put(actionCreator(FETCH_USER));
  yield put(actionCreator(FETCH_USER_PAGE, data));
}

function* watchUsers() {
  yield takeEvery(FETCH_USER, getUser);
  yield takeEvery(FETCH_USER_NEWS, getUserNews);
  yield takeEvery(FETCH_USER_PAGE, getPageUser);
  yield takeEvery(FETCH_USER_PAGE_NEWS, getPageUserNews);
  yield takeEvery(EDIT_USER, editUser);
}

export default function* rootSaga() {
  yield fork(watchFetchNews);
  yield fork(watchAuthorization);
  yield fork(watchUsers);
}
