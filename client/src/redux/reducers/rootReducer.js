import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import newsReducer from './newsReducer';
import usersReducer from './usersReducer';
import userPageReducer from './userPageReduce';

const rootReducer = combineReducers({
  news: newsReducer,
  user: usersReducer,
  form: formReducer,
  userPage: userPageReducer,
});

export default rootReducer;
