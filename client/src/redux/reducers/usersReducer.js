import { DELETE_USER, GET_USER, GET_USER_NEWS } from '../constant';

const initialState = { user: {} };

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return { ...state, user: action.payload };
    case GET_USER_NEWS:
      return { ...state, user: { ...state.user, news: action.payload } };
    case DELETE_USER:
      localStorage.removeItem('token');
      return initialState;
    default:
      return state;
  }
};

export default userReducer;
