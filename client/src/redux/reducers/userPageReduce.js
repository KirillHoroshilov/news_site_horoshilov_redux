import { GET_USER_PAGE, GET_USER_PAGE_NEWS } from '../constant';

const initialState = { user: {} };

const userPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER_PAGE:
      return { ...state, user: action.payload };
    case GET_USER_PAGE_NEWS:
      return { ...state, user: { ...state.user, news: action.payload } };
    default:
      return state;
  }
};

export default userPageReducer;
