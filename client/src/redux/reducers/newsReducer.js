import { GET_NEWS, FETCH_NEWS } from '../constant';

const initialState = { newsList: [] };

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NEWS:
      return { ...state, newsList: action.payload };
    case FETCH_NEWS:
      return state;
    default:
      return state;
  }
};

export default usersReducer;
