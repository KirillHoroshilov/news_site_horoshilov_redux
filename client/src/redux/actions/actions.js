const actionCreator = (type, payload = null) => (payload ? { type, payload } : { type });

export default actionCreator;
