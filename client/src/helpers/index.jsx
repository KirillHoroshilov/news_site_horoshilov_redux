import React from 'react';
import { TextareaAutosize, TextField } from '@material-ui/core';

export const renderInput = ({
  label,
  input: { onChange, onFocus, onBlur, onDragStart, onDrop, value, name },
  meta: { touched, invalid, error },
  type,
}) => (
  <TextField
    name={name}
    label={label}
    value={value}
    type={type}
    onChange={onChange}
    onFocus={onFocus}
    onBlur={onBlur}
    onDragStart={onDragStart}
    onDrop={onDrop}
    error={touched && invalid}
    helperText={touched && error}
  />
);

export const renderTextArea = ({
  label,
  input: { onChange, onFocus, onBlur, onDragStart, onDrop, value, name },
}) => (
  <TextareaAutosize
    style={{ width: '420px', height: '200px' }}
    name={name}
    label={label}
    placeholder={label}
    value={value}
    onChange={onChange}
    onFocus={onFocus}
    onBlur={onBlur}
    onDragStart={onDragStart}
    onDrop={onDrop}
    rowsMax={4}
  />
);

export const renderFileInput = ({ label, input: { onChange } }) => (
  <div>
    <label htmlFor="file__input">{label}</label>
    <div>
      <input
        id="file__input"
        type="file"
        accept=".jpg, .png, .jpeg"
        onChange={(event) => onChange(event.target.files[0])}
      />
    </div>
  </div>
);

export const getPath = (string) => {
  if (!string) {
    return false;
  }
  if (!string.includes('public')) {
    return string;
  }

  return `http://localhost:3001/${string.replace('public', '')}`;
};
